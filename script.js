// Function to make a GET request to the API and display data in HTML
function fetchDataAndDisplay() {
    // API endpoint
    const apiUrl = 'https://new.api.taag.co/profile?path=berkay';

    // Send a GET request
    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json(); // Parse the response as JSON
        })
        .then(data => {
            // Store the fetched data in a variable
            const profile = data.profile;

            // Access specific fields from the JSON data
            const imageBanner = profile.image_banner;
            const imageProfile = profile.image_profile;
            const name = profile.name;
            const job = profile.job;
            const email = profile.user_email;

            // Handle the data as needed
            const dataContainer = document.getElementById('data-container');
            dataContainer.innerHTML = `
            <div class="wrapper">
            <img class="banner-image" src="${imageBanner}" alt="Banner Image">
            <img class="profile-image" src="${imageProfile}" alt="Profile Image">
            <div class="text">
                <p class="name" style="font-weight: bold">${name}</p>
                <p class="job" style="color: grey"> ${job}</p>
                <p class="email" style="color: darkgre, font-wieght: bold">Email: ${email}</p>
                </div>
            </div>
            `;
        })
        .catch(error => {
            console.error('Error:', error);
        });
}

// Call the fetchDataAndDisplay function when the page loads
window.onload = fetchDataAndDisplay;
